/**
 * @format
 */

import { Navigation } from "react-native-navigation";


import { registerScreens } from './src/initScreen';
registerScreens();



Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
          stack: {
            options: {
              topBar: {
                visible: false,
              },
            },
            children: [
              {
                component: {
                  name: 'Home',
                },
              },
            ],
          },
        },
      });
});