import withPaperProvider from "./hoc/withPaperProvider.hoc";
import { Navigation } from "react-native-navigation";

export function registerScreens()
{
    Navigation.registerComponent("Home", () =>
    withPaperProvider(require("./screens/home").default)
    );
    Navigation.registerComponent("Dashboard", () =>
    withPaperProvider(require("./screens/dashboard").default)
  );
}
