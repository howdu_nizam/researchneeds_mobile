import React, {useEffect, useState} from 'react';
import {Appbar, Avatar, Divider, Button} from 'react-native-paper';
import {Navigation} from 'react-native-navigation';

import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

function Home(props) {
  return (
    <View style={{flex: 1}}>
      <Appbar.Header >
          
        <Appbar.Content
          style={{marginHorizontal: 25}}
          title={'Research Need'}
        />
          </Appbar.Header>
         
      <View style={{justifyContent: 'center', flex: 1, paddingHorizontal: 30}}>
        <Button
          mode="contained"
          onPress={() =>
            Navigation.push(props.componentId, {
              component: {
                name: 'Dashboard',
                options: {
                  topBar: {
                    title: {
                      text: 'Dashboard',
                    },
                  },
                },
              },
            })
          }>
          Dashboard
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
});

export default Home;
